#include "pthread_tools.h"
#include <sys/syscall.h>
#include <errno.h>
#include <stdio.h>
int gettid()
{
#ifdef SYS_gettid
     return syscall(SYS_gettid);
#endif
     return pthread_self();
}

void print_error(const char* message)
{
     char error_message[256] = {0};
     strerror_r(errno, error_message, sizeof(error_message));
     fprintf(stderr, "ERROR (sys/tools) %s: %s.\n", message, error_message);
}

int lock(pthread_mutex_t * mutex)
{
    int ret = pthread_mutex_lock(mutex);
    if(ret)
         print_error("pthread_mutex_lock");
    return ret;
}

int unlock(pthread_mutex_t * mutex)
{
    int ret = pthread_mutex_unlock(mutex);
    if(ret)
         print_error("pthread_mutex_unlock");
    return ret;
}

int wait_forever(pthread_cond_t *cond, pthread_mutex_t* mutex)
{
    int ret = pthread_cond_wait(cond, mutex);
    if(ret && ret != ETIMEDOUT)
         print_error("pthread_cond_wait");
    return ret;
}

int wait_a_sec(pthread_cond_t *cond, pthread_mutex_t* mutex, int timeout)
{
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec += timeout;
    int ret = pthread_cond_timedwait(cond, mutex, &ts);
    if(ret && ret != ETIMEDOUT)
         print_error("pthread_cond_wait");
    return ret;
}

int wake(pthread_cond_t *cond)
{
    int ret = pthread_cond_broadcast(cond);
    if(ret)
         print_error("pthread_cond_broadcast");
    return ret;
}
