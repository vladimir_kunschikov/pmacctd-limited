#pragma once
#include <stddef.h>
void buffer_set_output(int socket);
int buffer_add_data(char* data, size_t size);
void buffer_stop_thread();
