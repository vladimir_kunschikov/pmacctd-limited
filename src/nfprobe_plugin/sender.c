#include "sender.h"
#include "pthread_tools.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#define NF9_SOFTFLOWD_MAX_PACKET_SIZE   512
#define BUFFER_SIZE 1024*2*32*8

static pthread_mutex_t  mutex;
static pthread_mutex_t  alive_mutex;
static pthread_cond_t  condition;

typedef struct {
     uint16_t size;
     char data[NF9_SOFTFLOWD_MAX_PACKET_SIZE];
} NetworkDataUnit;
static NetworkDataUnit *buffer = NULL;

static size_t write_index = 0;
static size_t read_index = 0;
static int send_counter = 0;
static int started = 0;
static int alive = 1;
static int nfsock = -1;

pthread_t thread;
static void* sender(void*);
static void init_synchronization_primitives()
{
    if(pthread_cond_init(&condition, NULL))
         print_error("pthread_cond_init");
    if(pthread_mutex_init(&mutex, NULL))
         print_error("pthread_mutex_init");
}

static void init_buffer()
{
     size_t allocation_size = sizeof(NetworkDataUnit) * BUFFER_SIZE;
     printf("INFO [%d] (%s/%s), allocation of the %lu bytes\n", gettid(), __FILE__, __FUNCTION__, allocation_size);
     buffer = malloc(allocation_size); 
     if(buffer)
          memset(buffer, 0, allocation_size);
     else{
          fprintf(stderr, "ERROR [%d] (%s/%s), failed to allocate %lu bytes\n", gettid(), __FILE__, __FUNCTION__, allocation_size);
          return;
     }
}

static void start_thread(int timeout)
{
     static int t;
     t = timeout;
     if(pthread_create(&thread, NULL, sender, &t)){
          fprintf(stderr, "ERROR [%d] (%s/%s) failed to start sender\n", gettid(), __FILE__, __FUNCTION__);
          return;
     }
     started = 1;
}

void buffer_init_output(int socket, int timeout)
{
     nfsock = socket;
     if(started && buffer)
          return;

     init_synchronization_primitives();
     if(!buffer)
          init_buffer();

     if(!started)
          start_thread(timeout);
}

static int skipped_packet_counter = 0;
int buffer_add_data(char* data, size_t size)
{
     if(write_index == BUFFER_SIZE)
          write_index = 0;

     if(buffer[write_index].size){
          if(!(skipped_packet_counter%1000000))
               fprintf(stderr, "[%d] (%s/%s) skipped %d netflow packets\n", gettid(), __FILE__, __FUNCTION__, skipped_packet_counter + 1);
          skipped_packet_counter++;
          return 0;
     }

     lock(&mutex);
     memcpy(buffer[write_index].data, data, size);
     buffer[write_index].size = size;
     write_index++;
     wake(&condition);
     unlock(&mutex);
     return size;
}

static void send_buffer_remains()
{
  printf("INFO [%d] (%s/%s) started send of the buffer remains\n", gettid(), __FILE__, __FUNCTION__);
  int counter = 0;
     for(; buffer[read_index].size; usleep(1), counter++){
          if(send(nfsock, buffer[read_index].data, buffer[read_index].size, 0) == -1){
               print_error("failed to send netflow");
               return;
          }
          buffer[read_index].size = 0;
          read_index++;

          if(read_index == BUFFER_SIZE)
               read_index = 0;
     }
  printf("INFO [%d] (%s/%s) has sent %d remained flows\n", gettid(), __FILE__, __FUNCTION__, counter);
}

static void* sender(void* v)
{
     lock(&alive_mutex);
     int timeout = *(int*) v;
     printf( "[%d] (%s/%s) Netflow record transmition interval %d nanoseconds.\n", gettid(), __FILE__, __FUNCTION__, timeout);

     while(alive){
          if(read_index == BUFFER_SIZE)
               read_index = 0;

          lock(&mutex);
          if(!buffer[read_index].size){
               wait_a_sec(&condition, &mutex, 1);
               unlock(&mutex);
               continue;
          }
          unlock(&mutex);

          if(send(nfsock, buffer[read_index].data, buffer[read_index].size, 0) == -1){
               static time_t last_log_message = 0;
               if(time(0) > last_log_message + 10){
                    print_error("failed to send netflow");
                    last_log_message = time(0);
               }
          }else{
               if(!(send_counter%1000000))
                    printf("INFO [%d] (%s/%s) send counter %d\n", gettid(), __FILE__, __FUNCTION__, send_counter + 1);
               send_counter++;
               struct timespec ts = {.tv_sec = 0, .tv_nsec = timeout};
               nanosleep(&ts, NULL);
          }
          buffer[read_index].size = 0;
          read_index++;
     }

     unlock(&alive_mutex);
     send_buffer_remains();
     return NULL;
}

void buffer_stop_thread()
{
     lock(&mutex);
     alive = 0;
     wake(&condition);
     unlock(&mutex);
     if(!started)
          return;

     lock(&alive_mutex);
     unlock(&alive_mutex);
     if(pthread_join(thread, NULL))
          print_error("pthread_join");
     fprintf(stderr, "[%d] (%s/%s) rejected %d netflow records\n", gettid(), __FILE__, __FUNCTION__, skipped_packet_counter);
}
