#pragma once
#include <pthread.h>
int gettid();
void print_error(const char* message);
int lock(pthread_mutex_t * mutex);
int unlock(pthread_mutex_t * mutex);
int wait_forever(pthread_cond_t *cond, pthread_mutex_t* mutex);
int wait_a_sec(pthread_cond_t *cond, pthread_mutex_t* mutex, int timeout);
int wake(pthread_cond_t *cond);
